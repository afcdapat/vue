const app = Vue.createApp({
    data() {
        return {
            firstName: 'Notna',
            lastName: 'Tapad',
            age: '26',
            gender: 'male',
            email: 'notna.tpd@sample.com',
            photo: 'https://randomuser.me/api/portraits/men/87.jpg',
        }
    },
    methods: {
        async getUser() {
            const response = await fetch('https://randomuser.me/api/')
            const response_data = await response.json()

            console.log(response_data.results[0])

            this.firstName = response_data.results[0].name.first
            this.lastName = response_data.results[0].name.last
            this.gender = response_data.results[0].gender
            this.email = response_data.results[0].email
            this.photo = response_data.results[0].picture.large
        }
    },
})

app.mount('#app')